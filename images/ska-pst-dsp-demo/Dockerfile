ARG DSPDEMO_BASE_IMAGE=""
ARG DSPSR_BASE_IMAGE=""

FROM $DSPSR_BASE_IMAGE as dspsr
FROM $DSPDEMO_BASE_IMAGE as base

ENV DEBIAN_FRONTEND=noninteractive

ARG UID=1000
ARG GID=1000
ARG UNAME=pst

RUN groupadd -g $GID $UNAME  && \
    useradd -m -u $UID -g $GID -s /bin/bash $UNAME

ENV PSRHOME /home/${UNAME}/Pulsar
ENV PATH=${PATH}:${PSRHOME}/bin
ENV LD_LIBRARY_PATH=/usr/local/lib/:${PSRHOME}/lib

COPY ./dependencies /mnt/dependencies/
COPY --chown=${UID}:${GID} ./src/ska_pst_dsp_demo $PSRHOME/src/ska_pst_dsp_demo

RUN --mount=type=bind,from=dspsr,target=/mnt/dspsr \
    cp -rd /mnt/dspsr/${PSRHOME}/bin ${PSRHOME}/bin && \
    cp -rd /mnt/dspsr/${PSRHOME}/lib ${PSRHOME}/lib && \
    cp -rd /mnt/dspsr/${PSRHOME}/include ${PSRHOME}/include && \
    chown -R ${UID}:${GID} ${PSRHOME}

# Prepare environment
RUN apt-get update -y \
    && apt-get install -y --reinstall ca-certificates \
    && update-ca-certificates \
    && apt-get install -y make \
    && apt-get install --no-install-recommends -y `cat /mnt/dependencies/ska-pst-psrchive/apt.txt` \
    && apt autoremove

USER ${UNAME}

RUN find $PSRHOME

# Compile ska_pst_dsp_demo
WORKDIR $PSRHOME/src/ska_pst_dsp_demo
RUN mkdir build \
    && cd build \
    && cmake .. \
    && make -j$(nproc) \
    && make install \
    && rm -rf $PSRHOME/build $PSRHOME/src

# RUNTIME TEST
RUN ska_pst_dsp_demo -h

WORKDIR /home/${UNAME}
CMD ["/bin/bash"]
