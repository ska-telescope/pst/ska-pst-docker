# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


import os
import sys
import subprocess
import shutil
import textwrap

def setup(app):
    app.add_css_file('css/custom.css')

# -- Doxygen Generate --------------------------------------------------------

def configureDoxyfile(input_dir: str, output_dir: str):
    with open('../Doxyfile', 'r') as file:
        file_data = file.read()

    file_data = file_data.replace('@DOXYGEN_INPUT_DIR@', input_dir)
    file_data = file_data.replace('@DOXYGEN_OUTPUT_DIR@', output_dir)

    with open('../Doxyfile', 'w') as file:
        file.write(file_data)

# -- Project information -----------------------------------------------------

project = 'SKA PST DSP TOOLS'
copyright = '2023 Square Kilometre Array Observatory'
author = 'PST Team'

# The full version, including alpha/beta/rc tags
with open('../../.release') as f:
    version = f.readline().strip().split("release=")[1]
    release = version

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# Check if we're running on Read the Docs' servers
read_the_docs_build = os.environ.get('READTHEDOCS', None) == 'True'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'recommonmark',
]

source_suffix = [".rst", '.md']

from recommonmark.parser import CommonMarkParser
source_parsers = {".md": CommonMarkParser }

# Extra Config
input_dir = '../src'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages. See the documentation for
# a list of builtin themes.
#
html_theme = 'ska_ser_sphinx_theme'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
# html_theme_options = {}

html_context = {
    'github_repo': 'ska-pst-dsp-tools', #Repository name
    'gitlab_user': 'ska-telescope', #Repository name
    'gitlab_version': 'main',  #Version
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

