
# Changelog

## 0.0.13

- AT3-894 Update psrchive (2025-02-03), dspsr (2025-02-03), and .make
- AT3-894 fix ReadTheDocs build (poetry install --no-root)
- AT3-894 fix psrchive container build (define UID, GID and UNAME)
- AT3-894 documentation (README.md and ReadTheDocs) updated/corrected

## 0.0.12

- AT3-871 Update ska-pst-buildtools dependency to 0.0.11

## 0.0.11

- AT3-857 Update psrchive (2024-12-02), dspsr (2024-12-02) and .make to include fixes to dsp::WeightedTimeSeries needed for AA0.5 post-processing

## 0.0.10

- AT3-838 Update psrchive (2024-09-15), dspsr (2024-10-01), .make to include fixes in dsp::ASCIIObservation needed for AA0.5 post-processing

## 0.0.9

- AT3-812 Update ska-pst-buildtools dependency from 0.0.9 to 0.0.10

## 0.0.8

- Update psrchive, dspsr, .make, .pst (dspsr 2024-08-20 includes GPU-capable digidada)

## 0.0.7

- update psrchive to 2024-07-25 release
- add psrchive to PKG_CONFIG_PATH environment variable

## 0.0.6

- Remove psrdada sub-module dependency
- Update psrchive, dspsr, .make, .pst 

## 0.0.5

- allow DADABuffer to tolerate failure of SHM_LOCK in PSRDADA dada_cuda_dbregister

## 0.0.4

- introduced multi-stage builds for psrchive and dspsr
- changed installing path to /usr/local for tempo2, psrcat, psrchive and dspsr
- changed dev image to ska-pst-buildtools
- add dada, kat and uwb backends to DSPSR's backends build list

## 0.0.3

- updated psrchive and dspsr submodules to latest versions after fixing dspsr bug AT3-589

## 0.0.2

- updated psrchive and dspsr submodules to latest versions

## 0.0.1

- ska-pst-psrchive builds psrchive and all of its dependencies
- ska-pst-dspsr builds dspsr and psrdada on top of ska-pst-psrchive image
