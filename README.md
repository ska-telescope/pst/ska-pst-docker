# SKA PST DSP TOOLS

This project builds Docker images that can be used to process data recorded by the the Pulsar Timing instrument for SKA Mid and SKA Low.
Three containers are built:

 * ska-pst-psrchive - builds psrchive and its dependencies (tempo2, psrcat, and splinter) on top of other dependencies installed using `apt-get` (e.g. pgplot, fftw, cfitsio, ... see the full list in dependencies/ska-pst-psrchive/apt.txt)
 * ska-pst-dspsr-builder - builds dspsr on top of the ska-pst-psrchive image
 * ska-pst-dspsr - configures an interactive shell on top of the ska-pst-dspsr-builder image

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-pst-dsp-tools/badge/?version=latest)](https://developer.skao.int/projects/ska-pst-dsp-tools/en/latest/)

The documentation for this project, including common usage examples can be found at the SKA developer portal:  [https://developer.skao.int/projects/ska-pst-dsp-tools/en/latest/](https://developer.skao.int/projects/ska-pst-dsp-tools/en/latest/)

## Build Instructions

Clone this repo and submodules to your local file system

    git clone --recursive git@gitlab.com:ska-telescope/pst/ska-pst-dsp-tools.git

### Build Docker Images

To build the Docker images:

    make oci-build-all

### Build Documentation

To build the documentation: 

    make docs-build html

## License

See the LICENSE file for details.
