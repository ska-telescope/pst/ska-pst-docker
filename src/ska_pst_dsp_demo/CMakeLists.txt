
cmake_minimum_required(VERSION 3.8)

message(STATUS "Building SkaPstDspDemo")

# Project configuration, specifying version, languages,
# and the C++ standard to use for the whole project
set(PACKAGE_NAME SkaPstDspDemo)
set(DESCRIPTION "Demo of CMake C++ application linked to DSPSR with CUDA support")

project(${PACKAGE_NAME} VERSION 0.0.0 DESCRIPTION ${DESCRIPTION} LANGUAGES CUDA CXX)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

find_package(CUDAToolkit REQUIRED)
find_package(DSPSR REQUIRED)
find_package(PSRCHIVE REQUIRED)

add_executable(ska_pst_dsp_demo ska_pst_dsp_demo.cpp)

separate_arguments(PSRCHIVE_CFLAGS UNIX_COMMAND "${PSRCHIVE_CFLAGS}")
separate_arguments(PSRCHIVE_LDFLAGS UNIX_COMMAND "${PSRCHIVE_LDFLAGS}")
separate_arguments(DSPSR_CFLAGS UNIX_COMMAND "${DSPSR_CFLAGS}")
separate_arguments(DSPSR_LDFLAGS UNIX_COMMAND "${DSPSR_LDFLAGS}")

target_compile_options(ska_pst_dsp_demo
    PRIVATE
        ${PSRCHIVE_CFLAGS}
        ${DSPSR_CFLAGS}
)

target_link_options(ska_pst_dsp_demo
    PUBLIC
        ${DSPSR_LDFLAGS}
        ${PSRCHIVE_LDFLAGS}
)

target_link_libraries(ska_pst_dsp_demo
    PUBLIC
        dspbase dspdsp dspsr dspsrmore dspstats
        CUDA::cudart cufft
        psrutil
)

install(
    TARGETS
        ska_pst_dsp_demo
    DESTINATION
        bin
)
